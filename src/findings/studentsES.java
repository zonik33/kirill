package findings;

import database.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class studentsES {
    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    TextField newFIO;
    @FXML
    TextField newDate;
    @FXML
    TextField newAddress;
    @FXML
    TextField newPhone;

    @FXML
    TextField cod;

    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("студенты", "Код студента", arrValue);
        }
        if (!(newFIO.getText().equals(""))) {
            String[] arrValue = {newFIO.getText(), cod.getText()};
            editingDataBase.updateElementString("студенты", "ФИО студента", arrValue);
        }
        if (!(newDate.getText().equals(""))) {
            String[] arrValue = {newDate.getText(), cod.getText()};
            editingDataBase.updateElementString("студенты", "Дата рождения", arrValue);
        }
        if (!(newAddress.getText().equals(""))) {
            String[] arrValue = {newAddress.getText(), cod.getText()};
            editingDataBase.updateElementString("студенты", "Адрес", arrValue);
        }
        if (!(newPhone.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newPhone.getText()), Integer.parseInt(cod.getText())};
            editingDataBase.updateElementInt("студенты", "Телефон", arrValue);
        }
    }
}
