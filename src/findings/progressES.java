package findings;

import database.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class progressES {
    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    TextField newFIO;
    @FXML
    TextField newNameLesson;
    @FXML
    TextField newAssessment;
    @FXML
    TextField newFIOTeacher;
    @FXML
    TextField cod;
    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("успеваемость", "Код студента", arrValue);
        }
        if (!(newFIO.getText().equals(""))) {
            String[] arrValue = {newFIO.getText(), cod.getText()};
            editingDataBase.updateElementString("успеваемость", "ФИО студента", arrValue);
        }
        if (!(newNameLesson.getText().equals(""))) {
            String[] arrValue = {newNameLesson.getText(), cod.getText()};
            editingDataBase.updateElementString("успеваемость", "Название предмета", arrValue);
        }
        if (!(newAssessment.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newAssessment.getText()), Integer.parseInt(cod.getText())};
            editingDataBase.updateElementInt("успеваемость", "Оценка за семестр", arrValue);
        }
        if (!(newFIOTeacher.getText().equals(""))) {
            String[] arrValue = {newFIOTeacher.getText(), cod.getText()};
            editingDataBase.updateElementString("успеваемость", "ФИО преподавателя", arrValue);
        }
    }
}
