package findings;

import database.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class parentsES {
    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    TextField newStatusFamily;
    @FXML
    TextField newFIOMum;
    @FXML
    TextField newAddressMum;
    @FXML
    TextField newPhoneMum;
    @FXML
    TextField newWorkMum;
    @FXML
    TextField newFIODad;
    @FXML
    TextField newAddressDad;
    @FXML
    TextField newPhoneDad;
    @FXML
    TextField newWorkDad;
    @FXML
    TextField cod;

    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("родители", "Код студента", arrValue);
        }
        if (!(newStatusFamily.getText().equals(""))) {
            String[] arrValue = {newStatusFamily.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Полная семья", arrValue);
        }
        if (!(newFIOMum.getText().equals(""))) {
            String[] arrValue = {newFIOMum.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "ФИО мамы", arrValue);
        }
        if (!(newAddressMum.getText().equals(""))) {
            String[] arrValue = {newAddressMum.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Адрес мамы", arrValue);
        }
        if (!(newPhoneMum.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newPhoneMum.getText()), Integer.parseInt(cod.getText())};
            editingDataBase.updateElementInt("родители", "Телефон мамы", arrValue);
        }
        if (!(newWorkMum.getText().equals(""))) {
            String[] arrValue = {newWorkMum.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Место работы мамы", arrValue);
        }
        if (!(newFIODad.getText().equals(""))) {
            String[] arrValue = {newFIODad.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "ФИО папы", arrValue);
        }
        if (!(newAddressDad.getText().equals(""))) {
            String[] arrValue = {newAddressDad.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Адрес папы", arrValue);
        }
        if (!(newPhoneDad.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newPhoneDad.getText()), Integer.parseInt(cod.getText())};
            editingDataBase.updateElementInt("родители", "Телефон папы", arrValue);
        }
        if (!(newWorkDad.getText().equals(""))) {
            String[] arrValue = {newWorkDad.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Место работы папы", arrValue);
        }
    }

}
