package addData;

import database.Persn;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class Controle {
    Persn persn = new Persn();

    @FXML
    TextField cod;

    @FXML
    TextField FIO;

    @FXML
    TextField nameLesson;

    @FXML
    TextField assessment;

    @FXML
    TextField FIOteacher;
    /**
     * Добавляет строку в базу данных "Посещаемость"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        persn.connect();
        String sql = "INSERT `посещаемость` (`Код студента`,`ФИО студента`,`Название предмета`,`Оценка за семестр`, `ФИО преподавателя`) VALUES(" + Integer.parseInt(cod.getText()) + ",'" + FIO.getText() + "','" + nameLesson.getText() + "'," + Integer.parseInt(assessment.getText()) + ",'" + FIOteacher.getText() + "')";
        persn.stat.executeUpdate(sql);
        persn.close();
    }
}
