package addData;

import database.Persn;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class allStudents {
    Persn persn = new Persn();

    @FXML
    TextField codTF;

    @FXML
    TextField FIOTF;

    @FXML
    TextField dateTF;

    @FXML
    TextField addressTF;

    @FXML
    TextField phoneTF;

    /**
     * Добавляет строку в базу данных "Студенты"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */

    public void addLine() throws SQLException {
        persn.connect();
        String sql = "INSERT `студенты` (`Код студента`,`ФИО студента`,`Дата рождения`,`Адрес`, `Телефон`) VALUES(" + Integer.parseInt(codTF.getText()) + ",'" + FIOTF.getText() + "','" + dateTF.getText() + "','" + addressTF.getText() + "'," + Long.parseLong(phoneTF.getText()) + ")";
        persn.stat.executeUpdate(sql);
        persn.close();
    }
}
