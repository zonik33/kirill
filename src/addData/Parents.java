package addData;

import database.Persn;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class Parents {
    Persn persn = new Persn();

    @FXML
    TextField codTF;

    @FXML
    TextField statusFamilyTF;

    @FXML
    TextField FIOMumTF;

    @FXML
    TextField addressMumTF;

    @FXML
    TextField phoneMumTF;

    @FXML
    TextField workMumTF;

    @FXML
    TextField workDadTF;

    @FXML
    TextField phoneDadTF;

    @FXML
    TextField addressDadTF;

    @FXML
    TextField FIODadTF;
    /**
     * Добавляет строку в базу данных "Родители"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        persn.connect();
        String sql = "";
        switch (statusFamilyTF.getText()) {
            case "Полная":
                sql = "INSERT `родители` (`Полная семья`,`Код студента`,`ФИО мамы`,`Адрес мамы`,`Телефон мамы`,`Место работы мамы`,`ФИО папы`,`Адрес папы`,`Телефон папы`,`Место работы папы`) VALUES('" + statusFamilyTF.getText() + "'," + Integer.parseInt(codTF.getText()) + ",'" + FIOMumTF.getText() + "','" + addressMumTF.getText() + "'," + Long.parseLong(phoneMumTF.getText()) + ",'" + workMumTF.getText() + "','" + FIODadTF.getText() + "','" + addressDadTF.getText() + "'," + Long.parseLong(phoneDadTF.getText()) + ",'" + workDadTF.getText() + "')";
                break;
            case "Воспитывает Мама":
                sql = "INSERT `родители` (`Полная семья`,`Код студента`,`ФИО мамы`,`Адрес мамы`,`Телефон мамы`,`Место работы мамы`,`ФИО папы`,`Адрес папы`,`Телефон папы`,`Место работы папы`) VALUES('" + statusFamilyTF.getText() + "'," + Integer.parseInt(codTF.getText()) + ",'" + FIOMumTF.getText() + "','" + addressMumTF.getText() + "'," + Long.parseLong(phoneMumTF.getText()) + ",'" + workMumTF.getText() + "','" + null + "','" + null + "'," + null + ",'" + null + "')";
                break;
            case "Воспитывает Папа":
                sql = "INSERT `родители` (`Полная семья`,`Код студента`,`ФИО мамы`,`Адрес мамы`,`Телефон мамы`,`Место работы мамы`,`ФИО папы`,`Адрес папы`,`Телефон папы`,`Место работы папы`) VALUES('" + statusFamilyTF.getText() + "'," + Integer.parseInt(codTF.getText()) + "," + null + "," + null + "," + null + "," + null + ",'" + FIODadTF.getText() + "','" + addressDadTF.getText() + "'," + Long.parseLong(phoneDadTF.getText()) + ",'" + workDadTF.getText() + "')";
                break;
        }
        persn.stat.executeUpdate(sql);
        persn.close();

    }
}
