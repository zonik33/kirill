package addData;

import database.Persn;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class personalData {
    Persn persn = new Persn();

    @FXML
    TextField codTF;

    @FXML
    TextField passDataTF;

    @FXML
    TextField INNTF;

    @FXML
    TextField SNILSTF;

    /**
     * Добавляет строку в базу данных "Личные данные"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        persn.connect();
        String sql = "INSERT `личные данные` (`Код студента`,`Паспортные данные`,`ИНН`,`СНИЛС`) VALUES(" + Integer.parseInt(codTF.getText()) + "," + Integer.parseInt(passDataTF.getText()) + "," + Integer.parseInt(INNTF.getText()) + "," + Integer.parseInt(SNILSTF.getText()) + ")";
        persn.stat.executeUpdate(sql);
        persn.close();

    }
}
