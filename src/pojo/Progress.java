import database.Persn;
import database.EditingDataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojo.AcademicPerformanceTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Progress {
    Persn persn = new Persn();

    private ObservableList<AcademicPerformanceTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<AcademicPerformanceTable> tableView;

    @FXML
    private TableColumn<AcademicPerformanceTable, Integer> cod;

    @FXML
    private TableColumn<AcademicPerformanceTable, String> FIO;

    @FXML
    private TableColumn<AcademicPerformanceTable, String> nameLesson;

    @FXML
    private TableColumn<AcademicPerformanceTable, Integer> assessment;

    @FXML
    private TableColumn<AcademicPerformanceTable, String> FIOteacher;


    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        persn.connect();
        ResultSet resultSet = persn.stat.executeQuery("SELECT * FROM `успеваемость`");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<String> arrFIO = new ArrayList<>();
        ArrayList<String> arrNameLesson = new ArrayList<>();
        ArrayList<Integer> arrAssessment = new ArrayList<>();
        ArrayList<String> arrFIOteacher = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(2));
            arrFIO.add(resultSet.getString(3));
            arrNameLesson.add(resultSet.getString(4));
            arrAssessment.add(resultSet.getInt(5));
            arrFIOteacher.add(resultSet.getString(6));
        }
        persn.close();

        cod.setCellValueFactory(new PropertyValueFactory<AcademicPerformanceTable, Integer>("cod"));
        FIO.setCellValueFactory(new PropertyValueFactory<AcademicPerformanceTable, String>("FIOstud"));
        nameLesson.setCellValueFactory(new PropertyValueFactory<AcademicPerformanceTable, String>("nameLesson"));
        assessment.setCellValueFactory(new PropertyValueFactory<AcademicPerformanceTable, Integer>("assessment"));
        FIOteacher.setCellValueFactory(new PropertyValueFactory<AcademicPerformanceTable, String>("FIOteacher"));
        tableView.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new AcademicPerformanceTable(arrCod.get(i), arrFIO.get(i), arrNameLesson.get(i), arrAssessment.get(i), arrFIOteacher.get(i)));
        }

    }

    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField delete;
    public void deleteLine() throws SQLException {
        editingDataBase.deleteLine("успеваемость", "Код студента", Integer.parseInt(delete.getText()));

    }

    Stage primaryStage = new Stage();
    public void inputMenuEditing() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tablesediting/progress.fxml"));

        primaryStage.setTitle("Редактирование таблицы");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }


    public void inputMenuAdd() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("adddata/controle.fxml"));

        primaryStage.setTitle("Добавление данных в таблицу");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
