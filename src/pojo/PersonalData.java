import database.Persn;
import database.EditingDataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojo.PersonalDataTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PersonalData {
    Persn persn = new Persn();

    private ObservableList<PersonalDataTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<PersonalDataTable> tableView;

    @FXML
    private TableColumn<PersonalDataTable, Integer> cod;

    @FXML
    private TableColumn<PersonalDataTable, Integer> passData;

    @FXML
    private TableColumn<PersonalDataTable, Integer> INN;

    @FXML
    private TableColumn<PersonalDataTable, Integer> SNILS;


    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        persn.connect();
        ResultSet resultSet = persn.stat.executeQuery("SELECT * FROM `личные данные`");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<Integer> arrPassData = new ArrayList<>();
        ArrayList<Integer> arrINN = new ArrayList<>();
        ArrayList<Integer> arrSNILS = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(2));
            arrPassData.add(resultSet.getInt(3));
            arrINN.add(resultSet.getInt(4));
            arrSNILS.add(resultSet.getInt(5));
        }
        persn.close();

        cod.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Integer>("cod"));
        passData.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Integer>("passportData"));
        INN.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Integer>("INN"));
        SNILS.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Integer>("SNILS"));
        tableView.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new PersonalDataTable(arrCod.get(i), arrPassData.get(i), arrINN.get(i), arrSNILS.get(i)));
        }

    }

    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField delete;


    public void deleteLinePersonalData() throws SQLException {
        editingDataBase.deleteLine("личные данные", "Код студента", Integer.parseInt(delete.getText()));

    }

    Stage primaryStage = new Stage();


    public void inputMenuEditing() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tableg/attendance.fxml"));

        primaryStage.setTitle("Редактирование таблицы");
        primaryStage.setScene(new Scene(root, 600, 220));
        primaryStage.show();
    }


    public void inputMenuAdd() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ada/menu.fxml"));

        primaryStage.setTitle("Добавление данных в таблицу");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }


}
