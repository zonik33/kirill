import database.Persn;
import database.EditingDataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojo.ParentsTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class parents {
    Persn persn = new Persn();

    private ObservableList<ParentsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<ParentsTable> tableView;

    @FXML
    private TableColumn<ParentsTable, Integer> cod;

    @FXML
    private TableColumn<ParentsTable, String> fullFamily;

    @FXML
    private TableColumn<ParentsTable, String> FIOMum;

    @FXML
    private TableColumn<ParentsTable, String> addressMum;

    @FXML
    private TableColumn<ParentsTable, Integer> phoneMum;

    @FXML
    private TableColumn<ParentsTable, String> placeWorkMum;

    @FXML
    private TableColumn<ParentsTable, String> FIODad;

    @FXML
    private TableColumn<ParentsTable, String> addressDad;

    @FXML
    private TableColumn<ParentsTable, Integer> phoneDad;

    @FXML
    private TableColumn<ParentsTable, String> placeWorkDad;


    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        persn.connect();
        ResultSet resultSet = persn.stat.executeQuery("SELECT * FROM `родители`");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<String> arrFullFamily = new ArrayList<>();
        ArrayList<String> arrFIOMum = new ArrayList<>();
        ArrayList<String> arrAddressMum = new ArrayList<>();
        ArrayList<Integer> arrPhoneMum = new ArrayList<>();
        ArrayList<String> arrPlaceWorkMum = new ArrayList<>();
        ArrayList<String> arrFIODad = new ArrayList<>();
        ArrayList<String> arrAddressDad = new ArrayList<>();
        ArrayList<Integer> arrPhoneDad = new ArrayList<>();
        ArrayList<String> arrPlaceWorkDad = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(3));
            arrFullFamily.add(resultSet.getString(2));
            arrFIOMum.add(resultSet.getString(4));
            arrAddressMum.add(resultSet.getString(5));
            arrPhoneMum.add(resultSet.getInt(6));
            arrPlaceWorkMum.add(resultSet.getString(7));
            arrFIODad.add(resultSet.getString(8));
            arrAddressDad.add(resultSet.getString(9));
            arrPhoneDad.add(resultSet.getInt(10));
            arrPlaceWorkDad.add(resultSet.getString(11));

        }
        persn.close();

        cod.setCellValueFactory(new PropertyValueFactory<ParentsTable, Integer>("cod"));
        fullFamily.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("fullFamily"));
        FIOMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("FIOMum"));
        addressMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("addressMum"));
        phoneMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, Integer>("phoneMum"));
        placeWorkMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("placeWorkMum"));
        FIODad.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("FIODad"));
        addressDad.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("addressDad"));
        phoneDad.setCellValueFactory(new PropertyValueFactory<ParentsTable, Integer>("phoneDad"));
        placeWorkDad.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("placeWorkDad"));
        tableView.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new ParentsTable(arrCod.get(i), arrFullFamily.get(i), arrFIOMum.get(i), arrAddressMum.get(i), arrPhoneMum.get(i), arrPlaceWorkMum.get(i), arrFIODad.get(i), arrAddressDad.get(i), arrPhoneDad.get(i), arrPlaceWorkDad.get(i)));
        }

    }

    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField delete;

    public void deleteLineParents() throws SQLException {
        editingDataBase.deleteLine("родители", "Код студента", Integer.parseInt(delete.getText()));

    }

    Stage primaryStage = new Stage();


    public void inputMenuEditing() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("lelb/parent.fxml"));

        primaryStage.setTitle("Редактирование таблицы");
        primaryStage.setScene(new Scene(root, 600, 500));
        primaryStage.show();
    }

    public void inputMenuAdd() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("quit/parents.fxml"));

        primaryStage.setTitle("Добавление данных в таблицу");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
