import database.Persn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.allStudentsTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class allStudents {
    Persn persn = new Persn();

    private ObservableList<allStudentsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<allStudentsTable> tableView;

    @FXML
    private TableColumn<allStudentsTable, String> FIO;

    @FXML
    private TableColumn<allStudentsTable, String> date;

    @FXML
    private TableColumn<allStudentsTable, String> address;

    @FXML
    private TableColumn<allStudentsTable, Integer> phone;

    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        persn.connect();
        ResultSet resultSet = persn.stat.executeQuery("SELECT * FROM `студенты`");

        ArrayList<String> arrFIO = new ArrayList<>();
        ArrayList<String> arrDate = new ArrayList<>();
        ArrayList<String> arrAddress = new ArrayList<>();
        ArrayList<Integer> arrPhone = new ArrayList<>();
        while (resultSet.next()) {
            arrFIO.add(resultSet.getString(3));
            arrDate.add(resultSet.getString(4));
            arrAddress.add(resultSet.getString(5));
            arrPhone.add(resultSet.getInt(6));
        }
        persn.close();

        FIO.setCellValueFactory(new PropertyValueFactory<allStudentsTable, String>("FIO"));
        date.setCellValueFactory(new PropertyValueFactory<allStudentsTable, String>("date"));
        address.setCellValueFactory(new PropertyValueFactory<allStudentsTable, String>("address"));
        phone.setCellValueFactory(new PropertyValueFactory<allStudentsTable, Integer>("phone"));
        tableView.setItems(table);

        for (int i = 0; i < arrFIO.size(); i++) {
            table.add(new allStudentsTable(arrFIO.get(i), arrDate.get(i), arrAddress.get(i), arrPhone.get(i)));
        }

    }

}
