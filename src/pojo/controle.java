import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class controle {
    Stage primaryStage = new Stage();


    public void inputMenuTwo() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("menuTwo.fxml"));

        primaryStage.setTitle("Меню таблиц");
        primaryStage.setScene(new Scene(root, 250, 200));
        primaryStage.show();
    }


    public void inputAllStudents() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("allStudents.fxml"));

        primaryStage.setTitle("Все студенты");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public void inputAttendance() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("attendance.fxml"));

        primaryStage.setTitle("Посещаемость");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public void inputAcademicPerfomance() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("academicPerfomance.fxml"));

        primaryStage.setTitle("Успеваемость");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
