package test;


public class JavaTest {
    DataBaseEditing dataBaseEditing = new DataBaseEditing();
    BD bd = new BD();

    @Test
    public void addLineTwoValuesTest() throws SQLException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(6);
        arrayList.add("Литература");
        dataBaseEditing.addLineTwoValues("предметы", arrayList);
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM `предметы`");
        ArrayList<Integer> codIt = new ArrayList<>();
        ArrayList<String> nameIt = new ArrayList<>();
        while (resultSet.next()) {
            codIt.add(resultSet.getInt(1));
            nameIt.add(resultSet.getString(2));
        }
        bd.close();
        boolean checkOne = false;
        boolean checkTwo = false;
        int lostValue = codIt.size() - 1;
        if (codIt.get(lostValue) == 6) checkOne = true;
        if (nameIt.get(lostValue).equals("Предмет")) checkTwo = true;
        assertTrue(checkOne && checkTwo);
    }

    @Test
    public void deleteLineTest() throws SQLException {
        dataBaseEditing.deleteLine("предметы", "Код предмета", 6);
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM `предметы`");
        ArrayList<Integer> codIt = new ArrayList<>();
        ArrayList<String> nameIt = new ArrayList<>();
        while (resultSet.next()) {
            codIt.add(resultSet.getInt(1));
            nameIt.add(resultSet.getString(2));
        }
        bd.close();
        boolean checkOne = false;
        boolean checkTwo = false;
        int lostValue = codIt.size() - 1;
        if (codIt.get(lostValue) != 6) checkOne = true;
        if (!(nameIt.get(lostValue).equals("Предмет"))) checkTwo = true;
        assertTrue(checkOne && checkTwo);
    }

}